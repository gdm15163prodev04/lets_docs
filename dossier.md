#Productiedossier
_Klets!_
__Door Wesley Vanbrabant en Benjamin Lierman__
__Academiejaar 2015-2016__
__Grafische en digitale media__
__Multimediaproductie proDEV__
__Arteveldehogeschool__

*****

##Inhoudstafel
Briefing
Boards
	Ideaboard
	Moodboard
Style Tiles
	Style tile 1
	Style tile 2
	Style tile 3
Sitemap
Wireframes

*****

#Briefing
Klets! is een online web applicatie gemaakt in MVC6.
LETS is een eigentijdse interpretatie op ruilhanel. Aan de hand van diensten voor andere te doen, krijg je punten. Met deze punten koop je op jouw beurt diensten van anderen.
Onze web-app zorgt voor een algemene structuur om dit op een goede manier te laten verlopen.

#Analyse
Onze opdracht bestaat erin naast een LETS website te maken, ook enkele problemen die inherent zijn met LETS op te lossen.
Al vroeg beseften Wesley en mezelf dat dit een uitdagende opdracht zal zijn. Hoe verder we nadachten over de functionaliteiten en het design, hoe meer dingen beter moesten uitgedacht worden, moesten op elkaar afgestemd worden en dergelijke.

Dit was niet het enige, Wesley en ik moeste constant overal raad vragen over wat LETS nu juist was, hoe het werkt in de praktijk, hoe groepen in elkaar zaten, … Dit maakte er het proces niet makkelijker op.

Het leren werken met ASP.NET en MVC kan wel nog eens de grootste uitdaging zijn. Gelukkig hebben we vorig jaar al Laravel en ASP.net gezien, want daardoor begrijpen we al wat beter de mappenstructuur, de manier van denken, …

Hopelijk komt het werkstuk tot een goed einde!

#Tech Specs
HTML
CSS
JavaScript
CSS pre-processors
Static Site Generators
Commandline and Automation tools
JS bibliotheken & frameworks, zoals: jQuery, Normalize, Modernizr, Respond.js, Underscore.js, Lodash.js, Crossroads.js, Hasher.js, ...
DNX, DNU
ASP.NET 5
MVC6
Entity Framework 6+
...

#Func Specs
Login/Register
Live search
Admin panel
Grouping

#Persona Sheets
1. Dit is Kenny. Kenny is 22 jaar en een echte dierenliefhebber. Hij houd van honden, katten, noem maar op! In het dagelijkse leven is Kenny houtbewerker. Maar, al enige jaren passen Kenny en zijn vriendin samen op dieren van mensen die op reis zijn, een daguitstap doen. Deze taak varieert van de dieren enkel eten en drinken te geven, naar ermee gaan wandelen, … Hiervoor krijgen ze dan vaak een biermand, een bongobon, … Maar, laatst heeft iemand van zijn ‘klanten’ Kenny geïntroduceerd tot LETS. Nu in plaats van telkens te moeten een biermand opdrinken, krijgt Kenny hiervoor LETS-eenheden. Met die LETS-eenheden kan Kenny nu andere klusjes uit handen geven, waar hij zelf de tijd niet voor heeft. Onder andere kwam Geert (een vriendelijke ietswat oudere man, die toch nog mee is met zijn tijd) alle gevallen bladeren van de bomen samenharken en naar het containerpark voeren. Iets wat normaal heel veel geld zou gekost hebben, maar met LETS nu heel gemakkelijk gaat! LETS heeft Kenny zijn leven aanzienlijk makkelijker gemaakt. Waar Kenny vooral van houd is de gebruiksvriendelijkheid van de website. Hij kan deze zowel op zijn desktop als op zijn smartphone beheren. Zo is een vraag of aanbieding plaatsen heel gemakkelijk, en tot op de koop toe heeft Kenny nu nog meer de kans om op dieren te passen.



2. Dit is Maria. Maria is een iets wat oudere dame op pensioen. 5 jaar terug is ze haar Man verloren. Maar ,Maria blijft graag bezig. Met naaien (ze heeft dit toentertijd nog geleerd in school), met bakken, … Haar kleindochter weet dit en heeft Maria een link gestuurd op haar laptop naar LETS. Hedendaags is Maria druk in de weer met taarten bakken, met broeken en sokken herstellen, … voor iedereen uit haar buurt. Het is fijn voor Maria dat ze hiervoor LETS-punten krijgt ipv centjes (of zelfs helemaal niks), want met die punten kan ze kleine zwaardere klusjes ook laten doen door mensen uit haar buurt! Het is altijd een heel fijne bedoening! Want meestal blijven die mensen dan nog een koffietje drinken, en heeft Maria terug wat gezelschap! LET’s maakt het leven voor iedereen terug een heel stuk aangenamer!

3. Dit is Lucas. Lucas is een jonge tiener uit Roeselare. Zoals elke jonge gast is Lucas echt een krak met computers. Ze uit elkaar halen, virussen verwijderen, … noem het maar op, Lucas kan het. Toen Lucas overlaatst op reddit zat, zag hij een thread over LETS. Nieuwsgierig als hij was klikte hij door, en kwam hij terecht op onze website. Eventjes registreren en hop! Lucas plaatst aanbiedingen om overal computerproblemen op te lossen! Zowel bij oudere mensen als jongeren mag Lucas langsgaan om hun kleine personal computer probleempjes te fixen. In ruil daarvoor krijgt hij LETS-eenheden. Deze gebruikt Lucas dan terug om bijles frans te krijgen, van een meisje uit de buurt! Iets wat bij een bijlesbureau niet goedkoop is, maar dankzij LETS si het een heel informele bedoening, maar wel heel handig voor Lucas!

#Ideaboard
We gaan nog moeten de img in een map steken, en deze dan linken in onze MD

#Moodboard
We gaan nog moeten de img in een map steken, en deze dan linken in onze MD

#Sitemap

#Style Tiles
##Style tile 01
##Style tile 02
##Style tile 03

#Wireframes